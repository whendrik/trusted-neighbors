use indicatif::ProgressBar;
use indicatif::ProgressStyle;
use rand::Rng;

fn main() {
    let mut rng = rand::thread_rng();

    // Containing problem as graph
    let mut chain_of_trust: Vec<u16> = vec![];

    // Containing current state of trust, as binairy number
    let mut current_trust: [u16; 12] = [0; 12];

    // Set problem to Ponder This Feb
    set_chain(&mut chain_of_trust);
    //set_chain_42(&mut chain_of_trust);

    // Print the Grid
    print_grid(&chain_of_trust, &current_trust);

    // Does the current bit, trusts its (upper|lower|left|right) neighbor?
    // If so, this bit representation is 1 as well
    // Edges are always 0, as they are outside grid
    //
    let mut trust_up_grid: [u16; 12] = [0; 12];
    let mut trust_right_grid: [u16; 12] = [0; 12];
    let mut trust_down_grid: [u16; 12] = [0; 12];
    let mut trust_left_grid: [u16; 12] = [0; 12];

    initialize_trustbits(
        &chain_of_trust,
        &mut trust_up_grid,
        &mut trust_right_grid,
        &mut trust_down_grid,
        &mut trust_left_grid,
    );

    //
    // Do a run
    //
    run(
        &mut current_trust,
        &trust_up_grid,
        &trust_right_grid,
        &trust_down_grid,
        &trust_left_grid,
    );

    println!();
    print_grid(&chain_of_trust, &current_trust);

    // Easy Annealing Scheme
    let mut temp: f64 = 25.0;
    let alpha: f64 = 0.95;
    let stop_temp: f64 = 0.01;

    let mut current_init: [u16; 12] = [0; 12];
    let mut proposal_init: [u16; 12] = [0; 12];
    let mut proposal_trust: [u16; 12] = [0; 12];

    // 1000 is already more than enough
    // 1M is still 'waitable'
    let repeats = 1000;

    //let coolings = int( math.ceil(  math.log(T_stop / T) / math.log(alpha)  ) )

    let coolings = ((stop_temp / temp).log(alpha)).ceil() as u64;

    let bar = ProgressBar::new(coolings);
    bar.set_style(
        ProgressStyle::default_bar()
            .template("[{elapsed_precise}] {bar:40.red/yellow} {percent}% {msg}"),
    );

    for _ in 0..coolings {
        bar.inc(1);

        let mut accepted = 0;
        let mut obj_proposal: u32;
        let mut obj: u32 = 0;

        for _ in 0..repeats {
            for (i, trust_row) in current_init.iter().enumerate() {
                proposal_init[i] = *trust_row;
                current_trust[i] = *trust_row;
            }

            let current_convinced_start = number_of_convinced(&current_init);
            run(
                &mut current_trust,
                &trust_up_grid,
                &trust_right_grid,
                &trust_down_grid,
                &trust_left_grid,
            );
            let current_convinced_end = number_of_convinced(&current_trust);

            obj = objective(current_convinced_start, current_convinced_end);

            flip_a_bit(&mut proposal_init);

            if rng.gen::<f64>() < 0.5 {
                flip_a_bit(&mut proposal_init);
            }

            for (i, trust_row) in proposal_init.iter().enumerate() {
                proposal_trust[i] = *trust_row;
            }

            let proposal_convinced_start = number_of_convinced(&proposal_init);
            run(
                &mut proposal_trust,
                &trust_up_grid,
                &trust_right_grid,
                &trust_down_grid,
                &trust_left_grid,
            );
            let proposal_convinced_end = number_of_convinced(&proposal_trust);

            obj_proposal = objective(proposal_convinced_start, proposal_convinced_end);

            if metropolis_acceptance(obj, obj_proposal, temp) {
                accepted += 1;
                for (i, trust_row) in proposal_init.iter().enumerate() {
                    current_init[i] = *trust_row;
                }
                //print_state( &current_init );
            }
        }

        let current_convinced_start = number_of_convinced(&current_init);

        //println!("T: {}, M: {}, C: {}, O: {}", temp, (accepted as f64/ repeats as f64), current_convinced_start, obj_proposal);
        let message = format!(
            "T: {:.3}, M: {:.3}, C: {}, O: {}",
            temp,
            (accepted as f64 / repeats as f64),
            current_convinced_start,
            obj
        );
        bar.set_message(&message);

        temp *= alpha;
    }
    bar.finish();

    println!("---");
    println!(
        "number_of_convinced: {}",
        number_of_convinced(&current_init)
    );
    println!("---");
    print_grid(&chain_of_trust, &current_init);
    println!("---");
    for (i, trust_row) in current_init.iter().enumerate() {
        current_trust[i] = *trust_row;
    }
    run(
        &mut current_trust,
        &trust_up_grid,
        &trust_right_grid,
        &trust_down_grid,
        &trust_left_grid,
    );
    println!("---");
    print_grid(&chain_of_trust, &current_trust);
    println!("---");

    print!("[");
    for r in 0..12 {
        for c in 0..12 {
            let current_row = current_init[r];
            let current_bit = 1 & (current_row >> c);
            if current_bit > 0 {
                print!("({},{}),", (12 - c), (12 - r));
            }
        }
    }
    print!("]");
    println!("");
    println!("");
}

fn metropolis_acceptance(cost: u32, proposal_cost: u32, temp: f64) -> bool {
    if proposal_cost <= cost {
        return true;
    } else {
        let mut rng = rand::thread_rng();

        let luck = rng.gen::<f64>();

        let acceptance_probability =
            (-1.0 * ((proposal_cost as f64 - cost as f64) as f64) / temp).exp();

        return luck <= acceptance_probability;
    }
}

fn _switch_bits(current_trust: &mut [u16]) {
    let mut rng = rand::thread_rng();

    let random_row_1 = rng.gen_range(0..12);
    let random_col_1 = rng.gen_range(0..12);

    let random_row_2 = rng.gen_range(0..12);
    let random_col_2 = rng.gen_range(0..12);

    let random_bit_1 = 1 << random_col_1;
    let random_bit_2 = 1 << random_col_2;

    let bit_1 = (current_trust[random_row_1] & random_bit_1) >> random_col_1;
    let bit_2 = (current_trust[random_row_2] & random_bit_2) >> random_col_2;

    if bit_1 != bit_2 {
        current_trust[random_row_1] ^= random_bit_1;
        current_trust[random_row_2] ^= random_bit_2;
    }
}

fn flip_a_bit(current_trust: &mut [u16]) {
    let mut rng = rand::thread_rng();

    let random_row = rng.gen_range(0..12);
    let random_col = rng.gen_range(0..12);

    let random_bit = 1 << random_col;

    current_trust[random_row] ^= random_bit;
}

fn set_chain(chain_of_trust: &mut Vec<u16>) {
    *chain_of_trust = vec![
        0x0, 0xa, 0x8, 0x3, 0x0, 0x1, 0xb, 0x1, 0x1, 0xb, 0x0, 0x1, 0x1, 0xb, 0xd, 0xa, 0x4, 0x1,
        0xb, 0x2, 0x4, 0xd, 0x7, 0x8, 0x3, 0x7, 0xc, 0x0, 0x9, 0xe, 0x8, 0xd, 0x5, 0x9, 0x9, 0x8,
        0x6, 0x0, 0x4, 0x7, 0x3, 0x2, 0x8, 0x3, 0xd, 0x3, 0xb, 0x8, 0x1, 0x3, 0x2, 0x7, 0x9, 0x0,
        0x4, 0x3, 0xd, 0x9, 0xb, 0xc, 0x3, 0x7, 0x1, 0xb, 0xf, 0x4, 0xc, 0x0, 0x2, 0x1, 0xc, 0x1,
        0x1, 0xd, 0x1, 0x2, 0x2, 0xe, 0x8, 0x0, 0x0, 0xe, 0xe, 0x1, 0x5, 0xb, 0xc, 0x9, 0x6, 0x7,
        0x2, 0x6, 0x5, 0xd, 0x8, 0x8, 0x5, 0xf, 0x1, 0x9, 0x9, 0x8, 0xf, 0x5, 0x9, 0x1, 0x5, 0xd,
        0x6, 0x2, 0x8, 0xd, 0xf, 0xf, 0x0, 0x9, 0x4, 0x0, 0x3, 0x4, 0x3, 0x9, 0xe, 0xf, 0xf, 0xb,
        0xe, 0x6, 0xe, 0xc, 0xc, 0x8, 0x2, 0xc, 0x4, 0x4, 0x0, 0xc, 0x2, 0x0, 0xe, 0x0, 0xa, 0x0,
    ];
}

fn _set_chain_42(chain_of_trust: &mut Vec<u16>) {
    *chain_of_trust = vec![
        0x4, 0x1, 0x0, 0x4, 0x1, 0x0, 0x4, 0x1, 0x0, 0x4, 0x1, 0x0, 0x4, 0x1, 0x0, 0x4, 0x1, 0x0,
        0x4, 0x1, 0x0, 0x4, 0x1, 0x0, 0x4, 0x1, 0x0, 0x4, 0x1, 0x0, 0x4, 0x1, 0x0, 0x4, 0x1, 0x0,
        0x4, 0x1, 0x0, 0x4, 0x1, 0x0, 0x4, 0x1, 0x0, 0x4, 0x1, 0x0, 0x4, 0x1, 0x0, 0x4, 0x1, 0x0,
        0x4, 0x1, 0x0, 0x4, 0x1, 0x0, 0x4, 0x1, 0x0, 0x4, 0x1, 0x0, 0x4, 0x1, 0x0, 0x4, 0x1, 0x0,
        0x4, 0x1, 0x0, 0x4, 0x1, 0x0, 0x4, 0x1, 0x0, 0x4, 0x1, 0x0, 0x4, 0x1, 0x0, 0x4, 0x1, 0x0,
        0x4, 0x1, 0x0, 0x4, 0x1, 0x0, 0x4, 0x1, 0x0, 0x4, 0x1, 0x0, 0x4, 0x1, 0x0, 0x4, 0x1, 0x0,
        0x4, 0x1, 0x0, 0x4, 0x1, 0x0, 0x4, 0x1, 0x0, 0x4, 0x1, 0x0, 0x4, 0x1, 0x0, 0x4, 0x1, 0x0,
        0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
    ];
}

fn run(
    current_trust: &mut [u16],
    trust_up_grid: &[u16],
    trust_right_grid: &[u16],
    trust_down_grid: &[u16],
    trust_left_grid: &[u16],
) {
    let mut number_of_trust: u32 = 0;
    let mut last_trust: u32 = 0;

    tick(
        current_trust,
        &trust_up_grid,
        &trust_right_grid,
        &trust_down_grid,
        &trust_left_grid,
    );

    for trust_bits in current_trust.iter() {
        number_of_trust += trust_bits.count_ones();
    }

    while last_trust != number_of_trust {
        last_trust = number_of_trust;

        tick(
            current_trust,
            &trust_up_grid,
            &trust_right_grid,
            &trust_down_grid,
            &trust_left_grid,
        );

        number_of_trust = 0;
        for trust_bits in current_trust.iter() {
            number_of_trust += trust_bits.count_ones();
        }
    }
}

fn number_of_convinced(initial_trust: &[u16]) -> u32 {
    let mut number_of_trust: u32 = 0;

    for trust_bits in initial_trust.iter() {
        number_of_trust += trust_bits.count_ones();
    }

    return number_of_trust;
}

fn objective(convinced_at_start: u32, convinced_at_end: u32) -> u32 {
    let cost: u32 = 5 * (144 - convinced_at_end) + convinced_at_start;

    return cost;
}

fn tick(
    current_trust: &mut [u16],
    trust_up_grid: &[u16],
    trust_right_grid: &[u16],
    trust_down_grid: &[u16],
    trust_left_grid: &[u16],
) {
    for i in 0..12 {
        let trust_bits = current_trust[i];
        let right = trust_bits << 1;
        let left = trust_bits >> 1;

        let mut up: u16 = 0;
        let mut down: u16 = 0;

        if i > 0 {
            up = current_trust[i - 1];
        }
        if i < 11 {
            down = current_trust[i + 1];
        }
        //
        // 4095 = bx 111111111111
        //
        let new_state = 4095
            - ((trust_right_grid[i] & !right)
                | (trust_left_grid[i] & !left)
                | (trust_down_grid[i] & !down)
                | (trust_up_grid[i] & !up));

        current_trust[i] = trust_bits | new_state;
    }
}

fn initialize_trustbits(
    chain_of_trust: &[u16],
    trust_up_grid: &mut [u16],
    trust_right_grid: &mut [u16],
    trust_down_grid: &mut [u16],
    trust_left_grid: &mut [u16],
) {
    for c in 0..12 {
        let mut bits_of_trust_up: u16 = 0;
        let mut bits_of_trust_right: u16 = 0;
        let mut bits_of_trust_down: u16 = 0;
        let mut bits_of_trust_left: u16 = 0;

        for r in 0..12 {
            let trust = chain_of_trust[c * 12 + r];

            bits_of_trust_up += ((trust & 8) >> 3) << (11 - r);
            bits_of_trust_right += ((trust & 4) >> 2) << (11 - r);
            bits_of_trust_down += ((trust & 2) >> 1) << (11 - r);
            bits_of_trust_left += (trust & 1) << (11 - r);
        }

        // Upper row doesn't trust none-existing upper neighbors
        if c == 0 {
            bits_of_trust_up = 0;
        }
        // Down row doesn't trust none-existing down neighbors
        if c == 11 {
            bits_of_trust_down = 0;
        }
        // Most left- and right columns don't trust its none-existing
        // neighbors. This is done bit-wise;
        // 2047 = bx 011111111111
        // 4094 = bx 111111111110
        {
            bits_of_trust_left &= 2047;
            bits_of_trust_right &= 4094;
        }
        trust_up_grid[c] = bits_of_trust_up;
        trust_right_grid[c] = bits_of_trust_right;
        trust_down_grid[c] = bits_of_trust_down;
        trust_left_grid[c] = bits_of_trust_left;

        // println!("{:#014b}",bits_of_trust_left);
        // println!("{}",bits_of_trust_up);
    }
}

fn horizontal_arrows(left: u16, right: u16) {
    let both = (left << 1) + right;

    if both == 3 {
        print!("↔");
    }
    if both == 2 {
        print!("→");
    }
    if both == 1 {
        print!("←");
    }
    if both == 0 {
        print!(" ");
    }
}

fn vertical_arrows(up: u16, down: u16) {
    let both = (up << 1) + down;

    if both == 3 {
        print!("↕");
    }
    if both == 2 {
        print!("↓");
    }
    if both == 1 {
        print!("↑");
    }
    if both == 0 {
        print!(" ");
    }
    print!(" ");
}

fn _print_state(current_trust: &[u16]) {
    for (_i, trust_row) in current_trust.iter().enumerate() {
        println!("{:#014b}", trust_row);
    }
}

fn print_grid(chain_of_trust: &Vec<u16>, current_trust: &[u16]) {
    // 1000 : 8 - up
    // 0100 : 4 - right
    // 0010 : 2 - down
    // 0001 : 1 - left

    for (i, c) in chain_of_trust.iter().enumerate() {
        let shifter = 11 - (i % 12);
        let index = i / 12;

        if (current_trust[index] >> shifter) & 1 > 0 {
            print!("■");
        } else {
            print!("□");
        }

        if i < 143 {
            if !(i % 12 == 11) {
                let left = (*c & 4) >> 2;
                let right = chain_of_trust[i + 1] & 1;
                horizontal_arrows(left, right);
            }
            if i % 12 == 11 {
                println!();
                if i < 133 {
                    for d in 0..12 {
                        let up = (chain_of_trust[i - 11 + d] & 2) >> 1;
                        let down = (chain_of_trust[i + 1 + d] & 8) >> 3;

                        vertical_arrows(up, down);
                    }
                    println!();
                }
            }
        }
    }

    println!();
}
